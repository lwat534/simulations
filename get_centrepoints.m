function [centrePoints, tgt_vectors, centre_curve, deriv_spline] = ...
    get_centrepoints(ElemIds,ElemNodeIds,NodePos,n,plotting)
    %this function outputs a spline through the centreline of the stomach
    %in Matlab's ppform, and n points along that curve in as a 3=x array 
    % in nPoints.
    %NodePos, ElemIds, ElemNodeIds are all data read in about the
    %structure/coordinates of the stomach model
    %n is the number of centreline points desired
    %plotting is a boolean controlling whether or not to make a plot of
    %vectors created

if ~exist('n','var')
    n = 29;
end

if ~exist('plotting','var')
    plotting=0;
end

if plotting
    figure;hold on; grid on;
end

%preallocate array for initial centre points
centre_points_sparse = zeros(3,8);

%This code is modified from Recep Avci's test_centreline.m script
for iRing = 1:8
    %get mean of points in each ring as the centre point
    selElemIds = ElemIds((iRing-1)*8+1:iRing*8);
    selNodeIds = ElemNodeIds(selElemIds,:);
    centre_points_sparse(:,iRing) = [mean(NodePos(selNodeIds,1)), ...
             mean(NodePos(selNodeIds,2)), ...
             mean(NodePos(selNodeIds,3))]';
         
    %if desired, plot all stomach points and that centreline point
    if plotting
        scatter3(NodePos(selNodeIds,1), ...
            NodePos(selNodeIds,2), ...
            NodePos(selNodeIds,3), '.k');
    end
end

%get cupic spline in pp form
centre_curve = cscvn(centre_points_sparse);

%get points along curve
if n == 8
    points = centre_curve.breaks;
else
    points = linspace(centre_curve.breaks(1),centre_curve.breaks(end),n);
end

centrePoints = fnval(centre_curve,points);

%get tangent vectors
deriv_spline = fnder(centre_curve);
tgt_vectors = fnval(deriv_spline,points);

for v = 1:size(tgt_vectors,2)
    tgt_vectors(:,v) = tgt_vectors(:,v) / norm(tgt_vectors(:,v));
end


%make a plot
if plotting
    fnplt(centre_curve,'r')
    
    scatter3(centre_points_sparse(1,:),...
        centre_points_sparse(2,:),centre_points_sparse(3,:), ...
        'filled');
    
    scatter3(centrePoints(1,:),centrePoints(2,:),centrePoints(3,:),'x')
     
    quiver3(centrePoints(1,:),centrePoints(2,:),centrePoints(3,:),...
        tgt_vectors(1,:),tgt_vectors(2,:),tgt_vectors(3,:))
end

end