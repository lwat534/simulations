function [points_rotated, vectors_rotated] = transform_dipoles(points, vectors, ...
    theta_disp, midPoint)

%% get parameters

%rotation parameters in each dimension
theta_x = theta_disp(1);
theta_y = theta_disp(2);
theta_z = theta_disp(3);

%translation parameters in each dimension
disp_x = theta_disp(4);
disp_y = theta_disp(5);
disp_z = theta_disp(6);

%direction of dipoles (1 = anterograde direction, -1 = retrograde
%direction)
dir = theta_disp(7);


%% set up rotation matrices
rot_z = [cosd(theta_z), - sind(theta_z), 0;
        sind(theta_z), cosd(theta_z), 0;
        0,0,1];

rot_y = [cosd(theta_y), 0 ,sind(theta_y);
        0,1,0;
        -sind(theta_y), 0, cos(theta_y)];
        
    
rot_x = [1, 0, 0;
        0, cosd(theta_x), -sind(theta_x);
        0, sind(theta_x), cosd(theta_x)];

%% apply transformation

%rotate and translate points
points_rotated = rot_z * rot_y * rot_x * (points - midPoint);
points_rotated = points_rotated + midPoint;
points_rotated = points_rotated + [disp_x; disp_y; disp_z];


%rotate vectors and adjust direction
vector_end = vectors + points;
vectors_rotated = rot_z * rot_y * rot_x * (vector_end - midPoint);
vectors_rotated = vectors_rotated + midPoint - points_rotated;

vectors_rotated = vectors_rotated * dir;




end