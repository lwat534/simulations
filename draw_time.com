# For Lydia and Rachael
# Written by R. Avci
# March 2021
# Visualisation of simulations using CMISS

### Load some cmgui definitions to use in grahics
open com functions/cmgui_default exec 1;

$SID = 1
$OUT="output/output${SID}";

### Read geometry and sensor data
gfx read node stomach/stomach node_offset 1000;
gfx read elem stomach/stomach node_offset 1000 element_offset 1000 face_offset 1000;
gfx read elem stomach/grid node_offset 1000 element_offset 1000 face_offset 1000;

gfx read node $OUT/torso;
gfx read elem $OUT/torso;
gfx read data "$OUT/dipole${SID}";
gfx read data $OUT/squid data_offset 20000;

### Read potential (EGG) and magnetic field (MGG) data for a time step
$TIME = 11;
$TIME2 = sprintf("%05d",$TIME);
gfx read node $OUT/phi_$TIME2;
gfx read elem $OUT/phi;
gfx read data $OUT/mfi_known_$TIME2 data_offset 30000;

### Modify Torso/Stomach/Sensor/EGG/MGG grahical elements
gfx create spectrum phi_spect;
gfx create spectrum mfi_spect;
$ARROW_SCALE="0";

# Stomach geometry and source (dipole)
gfx mod g_elem stomach lines select_on mat dgrey;
gfx mod g_elem stomach surfaces select_on material pink;
gfx mod g_elem squid data glyph sphere general size "10*10*10" centre 0,0,0 mat purple invisible;
gfx mod g_elem dipole data glyph arrow_solid general size "20*20*20" centre 0,0,0 coordinate center.$TIME orientation orient.$TIME scale_factors "50*0*0" select_on mat dgreen;

# Torso geometry w/ body surface potentials
gfx mod g_elem torso lines select_on mat grey;
gfx mod g_elem torso surf data potential mat skin spectrum phi_spect;
gfx mod g_elem torso node glyph sphere data potential mat dred spectrum phi_spect;
gfx mod spectrum phi_spect autorange;

# Magnetic field data
gfx def field mag2 magnitude field mag
gfx mod g_elem mfi_known data_points  data mag2 glyph arrow_solid size "15*10*10" orient mag scale "4e06*1*1" centre 0,0,0 spectrum mfi_spect;
gfx mod spectrum mfi_spect autorange;

### Load default window settings
open com functions/cmgui_window exec 1;

### Loop over all the solutions and animate
$TSTART = 2;
$TEND = 29;
print "Type anim to animate for loop\n";	
sub anim
{
   for ($TIME=$TSTART;$TIME<=$TEND;$TIME += 1) 
   {
      $TIME2 = sprintf("%05d",$TIME);
      print "*** Reading phi_$TIME and mfi_known_$TIME2\n";
      gfx read node $OUT/phi_$TIME2;
      gfx read data $OUT/mfi_known_$TIME2 data_offset 30000;
      gfx def field mag2 magnitude field mag
      
      gfx mod g_elem mfi_known data_points data mag2 glyph arrow_solid size "15*10*10" orient mag scale "4e06*1*1" centre 0,0,0 spectrum mfi_spect;
      gfx mod spectrum mfi_spect autorange;
      
      # First delete the last dipole glyph and add a new one
      $PREV_TIME = $TIME-1;   
      gfx mod g_elem dipole data glyph arrow_solid general size "20*20*20" centre 0,0,0 coordinate center.$PREV_TIME orientation orient.$PREV_TIME scale_factors "50*0*0" select_on mat dgreen delete;
      gfx mod g_elem dipole data glyph arrow_solid general size "20*20*20" centre 0,0,0 coordinate center.$TIME orientation orient.$TIME scale_factors "50*0*0" select_on mat dgreen;

      gfx update window 1
      
      # Save image
	  #gfx print file image/T$TIME.png width 1000 height 1000 format rgb  
   }
}
