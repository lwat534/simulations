Project Steps:

1) Create dipole sources and write source files.
	- For a single time step, a single dipole source will be defined.
    - Dipoles are usually generated as a result of slow wave simulations.
    - However, we will simplify that approach to create static sources.
	- Dipoles will be positioned and oriented on the stomach surface.
	- Will use Matlab to do this.
	- See the example code [example_write_source.m]
	- Will discuss this further.
    - Please note that there is initially no source file in the folder named 'input'.
    - If you run example_write_source.m, a source file named dipole.ipsour will be written in that folder.
    - Try to run the code and see the source file structure.
    - We will be creating thousands of such file for different source parameters.

2) Simulate EGG and MGG using CM (aka CMISS)
	- This will be straightforward once (1) is done but hard to understand every step.
	- However, you do not need to totally understand every line of the code.
	- See the example code named example_torso_mf.com
	- Just try to learn how to run it for now. That would be sufficient for you. I want you to devote your time more on deep learning side.
	- The example code creates 3 new folders and writes outcomes in to these folders:
		- output: contains geometrical data to use in visualisation
		- output_V: body surface potential data
		- output_B: magnetic field data
	- For each time point, one output file was written for both B and V.  
	- To run the sims:
		- connect to hpc server. [ssh UPI@hpc2.bioeng.auckland.ac.nz]
		- download/trasnfer the folder named 20061116 (first time only)
		- navigate to your hpc folder (not 'people' folder) [cd /hpc/UPI]
		- run the command [/people/lche027/stefan/cm_20171004 example_torso_mf.com]
	- To visualise the simulation results using CMGUI
		- run the command [cmgui-motif draw.com]
		- will require X server to visualise.
		- will not work on your personal computer.
		- ignore for now and check the image folder that I have created for you. will go over these images together.
		
3) Deep learning
	- Will focus later.
