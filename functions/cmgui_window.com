#
# Hides axes visualisation and opens window 1
#

#gfx set vis axes off
gfx cre win 1;
gfx mod win 1 image view;
gfx mod win 1 set slow;
gfx mod win 1 set pert;
gfx mod win 1 layout width 600 height 600;
gfx mod win 1 background colour 1 1 1 all


