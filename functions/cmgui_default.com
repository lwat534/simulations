#
# Offset routine (inputs $GROUP) requires global variable $OFFSET;
#
sub myoffset()
{
    my( $GROUP ) = @_;
    print "  Offsetting group: $GROUP by $OFFSET\n";
    gfx change_id group $GROUP data_off $OFFSET face_off $OFFSET line_off $OFFSET node_off $OFFSET element_off $OFFSET;
    $OFFSET+=100000;
}

sub myprint()
{
#    $FILE="myprintfile";
    my ( $FILE ) = @_;
    print "Printing file - > ${FILE}.jpg \n";
    gfx print rgb format rgb file "/tmp/leo_test.rgb" anti 8;
    system ("convert -quality 80 /tmp/leo_test.rgb $FILE.jpg");
}



#
# Sets up default colours and axes visualisation
#

gfx cre mat aqua     diff 0.2 0.8 1.0 spec 1.0 1.0 1.0 shin 1.0 alpha 1.0
gfx cre mat red      diff 1.0 0.0 0.0 spec 0.8 0.4 0.4 shin 1.0 alpha .9
gfx cre mat dred     diff 1.0 0.0 0.0 spec 0.6 0.3 0.3 shin 1.0
gfx cre mat pred     diff 1.0 0.0 0.0 spec 1.0 0.0 0.0 shin 1.0 alpha .5
gfx cre mat pink     ambient 1 1 1 diffuse 0.8 0.3 0.3 emission 0 0 0 specular 0.28 0.23 0.36 alpha 1 shininess 1;
gfx cre mat pink_trans ambient 1 1 1 diffuse 0.8 0.3 0.3 emission 0 0 0 specular 1 1 1 alpha 0.3 shininess 1;

gfx cre mat blue     diff 0.1 0.2 0.7 spec 0.4 0.4 0.5 shin 0.5 
gfx cre mat dblue    diff 0.1 0.1 0.5 spec 1.0 1.0 1.0 shin 0.5 ambi 0 0 0
gfx cre mat pblue    diff 0.1 0.2 0.7 spec 0.4 0.4 0.5 shin 0.5 alpha .5
gfx cre mat green    diff 0.0 1.0 0.0 
gfx cre mat dgreen   diff 0.0 0.5 0.0 spec 0.4 0.4 0.5 shin 0.5 

gfx cre mat pgreen   diff 0.6 0.9 0.6 spec 0.5 0.5 0.5 shin 0.8 alph 0.6
gfx cre mat purple   diff 0.5 0.0 0.8 spec 0.4 0.5 0.0 shin 1.0 
gfx cre mat vaio     diff 0.6 0.3 0.8 spec 1.0 1.0 1.0 shin 0.5

gfx cre mat yellow   diff 1.0 0.6 0.0 alpha 0.5
gfx cre mat orange   diff 1.0 0.3 0.0 spec 1.0 1.0 1.0 shin 1.0 
gfx cre mat porange  diff 1.0 0.5 0.0 spec 1.0 1.0 1.0 shin 1.0  
gfx cre mat brown     diff 0.6 0.2 0.1 spec 0.4 0.3 0.3 ambi 0.4 0.4 0.4 shin 0.7
gfx cre mat pbrown     diff 0.6 0.2 0.1 spec 0.4 0.3 0.3 ambi 0.4 0.4 0.4 shin 0.7 alph 0.4

gfx cre mat gold     diff 1.0 0.7 0.0 spec 0.5 0.5 0.5 ambi 1.0 0.7 0.0 shin 0.8
gfx cre mat skin     diff 0.9 0.7 0.5 spec 1.0 1.0 1.0 shin 0.1 alph 0.5
gfx cre mat beige    diff 1.0 0.8 0.7 spec 1.0 0.6 0.6 shin 1.0
gfx cre mat ghost    diff 1.0 1.0 1.0 alpha 0.15 
gfx cre mat invisible    diff 1.0 1.0 1.0 alpha 0
gfx cre mat grey     diff 0.6 0.6 0.6 
gfx cre mat pgrey    diff 1.0 1.0 1.0 spec 0.3 0.3 0.3
gfx cre mat dgrey    diff 0.2 0.2 0.2 
gfx cre mat black    diff 0.0 0.0 0.0
gfx cre mat dblack   diff 0.0 0.0 0.0 spec 0 0 0 emi 0 0 0 ambi 0 0 0
gfx cre mat white    diff 1.0 1.0 1.0
gfx cre mat alpha    ambient 1 1 1 diffuse 1 1 1 emission 0 0 0 specular 0 0 0 alpha 0.4 shininess 0;
gfx cre mat SKIN alpha 0.2 ambient 0.48 0.29 0.41 diffuse 0.19 0.24 0.23 emission 0.41 0.36 0.5 specular 0.28 0.23 0.36;

#gfx set axis_length   60
#gfx set axis_material green
#gfx set vis axes on

