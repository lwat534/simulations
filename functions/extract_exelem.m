function [Elems, Nodes, Faces] = extract_exelem(fpath)
    fid = fopen(fpath,'r');
    txt = textscan(fid,'%s','delimiter', '\n'); txt = txt{1}; % read lines and unbox
    fclose(fid);
    ShapeLine = find(contains(txt, 'Dimension=3')==1);
    txt = txt(ShapeLine:end);
    
    ElemLineIds = find(contains(txt, 'Element')==1);
    Elems = [];
    Nodes = [];
    Faces = [];
    for iElem = 1:length(ElemLineIds)
        ElemLine = txt(ElemLineIds(iElem));
        ElemId = regexp(ElemLine, '\d+', 'match');
        ElemId = str2double(ElemId{1});

        Elems = [Elems; ElemId(ElemId~=0)];
        if iElem == length(ElemLineIds)
            subtxt = txt(ElemLineIds(iElem):end);
        else
            subtxt = txt(ElemLineIds(iElem):ElemLineIds(iElem+1)-1);
        end
        NodeLineId = find(contains(subtxt, 'Nodes')==1);
        NodeLine = subtxt{NodeLineId+1};
        NodeIds = regexp(NodeLine, '\d+', 'match');
        Nodes = [Nodes; str2double(NodeIds)];
        FaceLineId = find(contains(subtxt, 'Faces')==1);
        FaceLines = subtxt(FaceLineId+1:NodeLineId-1);
        FaceIds = cell2mat(cellfun(@(x) str2double(regexp(x, '\d+', 'match')), FaceLines, 'Un', 0));
        Faces= [Faces; FaceIds(FaceIds~=0)'];
    end
    %Elems = Elems(:,sum(Elems==0)==0);
end