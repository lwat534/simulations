function [FieldData] = extract_field_generic(fpath)
    % Reads .exnode or .exdata files and extracts field data from the file and
    % returns a structure array of FieldData as following:
    % FieldData.FieldName -> fields name in the file
    % FieldData.FieldComponents -> components of each field
    % FieldData.NodeIds -> Node ids correponding to each field value
    % FieldData.FieldValues -> field values from the file
    % R. Avci (2019)
    tic
    fid = fopen(fpath,'r');
    txt = textscan(fid,'%s','delimiter', '\n'); txt = txt{1}; % read lines and unbox
    fclose(fid);
    FieldLine = find(contains(txt, '#Fields')==1);
    if length(FieldLine) == 1
        nField = str2double(regexp(txt{FieldLine}, '\d+', 'match'));
    else
        disp("Something is wrong with the file format")
        return
    end
    iField = nField;
    NodeLineIds = contains(txt,'Node:')==1;
    NodeLines = txt(NodeLineIds);
    NodeIds = regexp(NodeLines, '\d+', 'match');
    NodeIds = str2double([NodeIds{:}])';
    Comps = {};
    CompValIdx = [];
    while iField > 0
        Pattern = sprintf('%d%s', iField, ') ');
        iLine = find(contains(txt, Pattern)==1);
        if length(iLine)==1
            line = txt{iLine};
        else
            disp("Something is wrong with the file format")
            return
        end
        FieldName = strsplit(line, ','); FieldName = FieldName{1};
        FieldName = strsplit(FieldName, ' ');
        FieldName = FieldName{2};
        LineSeg = line(strfind(line, '#Components='):end);
        nComp = str2num(cell2mat(regexp(LineSeg, '\d+', 'match')));
        for iComp = 1:nComp
            line = txt{iLine+iComp};
            lineparts = strsplit(line, '.');
            Comps{iField,iComp} = lineparts{1};
            valueidxpart = strsplit(lineparts{2}, ',');
            CompValIdx(iField,iComp) = str2double(regexp(valueidxpart{1}, '\d+', 'match'));
        end

        FieldData(iField).FieldName = FieldName;
        FieldData(iField).FieldComponents = Comps(iField,:);
        FieldData(iField).NodeIds = NodeIds;
        FieldData(iField).FieldValues = [];
        iField = iField-1;
    end
    
    NodeLineIds2 = find(NodeLineIds==1);
    LineDiff = unique(diff(NodeLineIds2));
    if length(LineDiff) == 1
        for iNode = 1:length(NodeLineIds2)
            NodeData = txt(NodeLineIds2(iNode)+1:NodeLineIds2(iNode)+LineDiff-1);
            NodeFieldValuesAll = cellfun(@(x) str2double(strsplit(x, ' ')), ...
                          NodeData, 'Un', 0);
                      
            NodeFieldValues = [];
            for iRow = 1:size(NodeFieldValuesAll)
                NodeFieldTmp = NodeFieldValuesAll{iRow};
                NodeFieldValues = [NodeFieldValues, NodeFieldTmp];
            end
                      
            for iField = 1:length(FieldData)
                 FieldData(iField).FieldValues = [FieldData(iField).FieldValues;
                                                  NodeFieldValues(CompValIdx(iField,:))]; 
            end
        end
    else
        disp("Something is wrong with the file format")
        return
    end
    time = toc;
    disp(strcat(fpath, " has been read in ", num2str(round(time)), ' seconds.')); 
end
