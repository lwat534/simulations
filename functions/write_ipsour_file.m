function write_ipsour_file(datain)
    % input: a structure array called datain with following inputs:
    %       - fname
    %       - dipole_pos (Nx3xT)
    %       - dipole_vec (Nx3xT)
    %Written by Recep Avci, with modifications by Lydia Watson
    datain = preprocess_input_file(datain);
    fname = datain.fname;
    dipole_pos = datain.dipole_pos;
    dipole_vec = datain.dipole_vec;
    
    % Define/identify the number of time steps
    nTimeStep = size(dipole_pos,3);
    % Define/identify the number of dipoles
    nDipole = size(dipole_pos,1);
    
    % Start writing
    fid = fopen(fname, 'w');
    line = sprintf(" CMISS Version 2.1  ipsour File Version 1");
    fprintf(fid, '%s\n', line);
    line = sprintf(" Heading:");
    fprintf(fid, '%s\n', line);
    line = sprintf(' ');
    fprintf(fid, '%s\n', line);
    
    line = sprintf('%s%7.0f', ' The number of dipole sources in region 1 is [1]:', nDipole);
    fprintf(fid, '%s\n', line);
    line = sprintf(' ');
    fprintf(fid, '%s\n', line);
    
    for iDipole = 1:nDipole
        line = sprintf('%s%7.0f%s', ' Does the centre of dipole', iDipole, ' move [N]? Y');
        fprintf(fid, '%s\n', line);
        line = sprintf('%s%7.0f%s', ' Does the dipole vector of dipole', iDipole, ' move [N]? Y');
        fprintf(fid, '%s\n', line);    
        line = sprintf(' ');
        fprintf(fid, '%s\n', line);
    
        line = sprintf('%s%7.0f', ' Enter the number of time points in the centre path [1]:', nTimeStep);
        fprintf(fid, '%s\n', line);
        line = sprintf(' ');
        fprintf(fid, '%s\n', line);
        line = sprintf('%s%7.0f%s%13.4E%12.4E%12.4E', ...
                       ' Enter the initial dipole centre for dipole', ...
                       iDipole, ':', dipole_pos(iDipole,1), dipole_pos(iDipole,2), dipole_pos(iDipole,3));
        fprintf(fid, '%s\n', line);
        for iTime = 1:nTimeStep
            line = sprintf('%s%7.0f%s%7.0f%s%13.4E', ...
                           ' Enter the time (in s) for time point', ...
                           iTime, 'for dipole', iDipole, ' centre:', iTime);
            fprintf(fid, '%s\n', line);
            line = sprintf('%s%7.0f%s%7.0f%s%13.4E%12.4E%12.4E', ...
                           ' Enter the dipole centre at time point', ...
                           iTime, ' for dipole', iDipole, ':', ...
                           dipole_pos(iDipole,1,iTime), ...
                           dipole_pos(iDipole,2,iTime), ...
                           dipole_pos(iDipole,3,iTime));
            fprintf(fid, '%s\n', line);
        end
        line = sprintf(' ');
        fprintf(fid, '%s\n', line);
        line = sprintf('%s%7.0f%s', ...
                       ' Enter the number of time points in the vector path [1]:', ...
                       nTimeStep);
        fprintf(fid, '%s\n', line);           
        line = sprintf(' ');
        fprintf(fid, '%s\n', line);
        
        line = sprintf('%s%7.0f%s%13.4E%12.4E%12.4E', ...
                       ' Enter the initial dipole vector p for dipole', ...
                       iDipole, ':', dipole_vec(iDipole,1), dipole_vec(iDipole,2), dipole_vec(iDipole,3));
        fprintf(fid, '%s\n', line);
        for iTime = 1:nTimeStep
            line = sprintf('%s%7.0f%s%7.0f%s%13.4E%12.4E%12.4E', ...
                           ' Enter the time (in s) for time point', ...
                           iTime, ' for dipole', iDipole, ' vector:', iTime);
            fprintf(fid, '%s\n', line);
            line = sprintf('%s%7.0f%s%7.0f%s%13.4E%12.4E%12.4E', ...
                           ' Enter the dipole vector p at time point', ...
                           iTime, ' for dipole', iDipole, ':', ...
                           dipole_vec(iDipole,1,iTime), ...
                           dipole_vec(iDipole,2,iTime), ...
                           dipole_vec(iDipole,3,iTime));
            fprintf(fid, '%s\n', line);
        end
    end
    fclose(fid);

    function datain = preprocess_input_file(datain)
        try
            fnametmp = datain.fname;
            if ~contains(fnametmp, 'ipsour')
                datain.fname = [fnametmp '.ipsour'];
            end
        catch
            datain.fname = 'dipole.ipsour';
        end
    end
end