### Define directories
$IN = "input"
unless (-d output) {mkdir output};
$OUT = "output/output${SID}"; unless (-d $OUT) {mkdir $OUT};

### Define variables
$TORSO = 1;
$LAPLACE = 1;
$FREQ = 1;
$TSTART= 0;
$TEND = 29;
$TSTEP = 1;
$OFFSET = 0;
$SQUIDOFFSET = 10000;

### Simulation 
# Define simulation parameters
fem define parameters;r;$IN/torso;
# Define coordinates (cartesian, 3D)
fem define coordinates 3,1;
# Define basis functions
fem define bases;r;$IN/torso_cubic;

# Define torso geometry: node, elements (removed the heart geometry)
fem define nodes;r;$IN/torso;
fem define elements;r;$IN/torso;

# Export geometry, .exnode, .exelem
fem export nodes;$OUT/torso as torso region $TORSO offset $OFFSET;
fem export elements;$OUT/torso as torso region $TORSO offset_elem $OFFSET;

# Group nodes to be applied for the .ipinit.
fem group nodes 1 as BOTTOM region $TORSO;
fem group nodes 254 as TOP region $TORSO;;
fem group nodes 2..253 as MIDDLE region $TORSO;

# Define laplace problem
fem define equation;r;$IN/torso region $TORSO class $LAPLACE;

# Define conductivity, i.e., material parameters
fem define material;r;$IN/torso region $TORSO class $LAPLACE;

# Define boundary conditions
fem define initial;r;$IN/torso region $TORSO class $LAPLACE;

# Define numerical solver
fem define solve;r;$IN/LU region $TORSO class $LAPLACE;

# Define dipole source
fem def source;r;$IN/dipole${SID} scale 1;
fem export source;$OUT/dipole${SID} as dipole;

# Read squid data and transform to our coordinates system
fem def data;r;$IN/squid176 field num_field 3;
fem change data rotate by 90,0,0 about 1,0,0;
fem change data trans  by -40,-250, 0
fem exp data;$OUT/squid as squid offset $SQUIDOFFSET error;

# Solve
for ($TIME=$TSTART;$TIME<=$TEND;$TIME+=$TSTEP)
{
    $FILENAME=sprintf("%05d",$TIME);
    #printf("*** Potential solve at time= $TIME\n");
    #printf("*********************************\n");

    fem update source region $TORSO class $LAPLACE time $TIME;
    fem solve at $TIME;
	#fem up phi tstart $TIME tend $TIME from nodes phi;
	fem export node;"$OUT/phi_$FILENAME" as torso field reg 1 class 1;

    #printf("\n\n");
    #printf("*** Magnetic solve at time= $TIME\n");
    #printf("*********************************\n");

	# Gradient MF
	fem eval solu mag from MFI nss 1 time $TIME freq $FREQ squid_conf 0 both baseline 50;
    fem up data field from MFI nss 1 time $TIME freq $FREQ;
    fem export data;$OUT/mfi_known_$FILENAME as mfi_known field_num 1..3 field_name mag offset $SQUIDOFFSET;
}
fem export elem;"$OUT/phi" as torso field reg 1 class 1;
quit
