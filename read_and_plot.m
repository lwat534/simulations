%% Written by L. Watson
% Adapted from example_write_source.m written by R. Avco
% March 2021
% Source generation for electrophys simulations
%%
clear;clc;close all;
addpath("functions")

%% Read stomach data
fpath = 'stomach/stomach.exnode';
[FieldData] = extract_field_generic(fpath);
StomNodePos = FieldData.FieldValues;


fpath = 'stomach/stomach.exelem';
%elemNodeIds gives the nodes (with coords contained in field data)for the
%each element
[ElemIds, ElemNodeIds, ElemFaces] = extract_exelem(fpath);

%% read torso data
torsoPath = 'output/output1/torso.exnode';
[TorsoField] = extract_field_generic(torsoPath);
TorsoNodePos = TorsoField.FieldValues;

%make an 'alpha shape' defining all torso points
torsoShp = alphaShape(TorsoNodePos,200);


%% read SID params

SID_params = readmatrix('SID_log.csv','NumHeaderLines',1);
SID_params = SID_params(:,2:end);


%% get centreline points and tangent vectors
nPoints = 29;

[centrePoints, tgt_vectors, centre_curve, deriv_spline] = ...
    get_centrepoints(ElemIds,ElemNodeIds,StomNodePos,nPoints);


%% make general plot of torso
figure(2)
hold off
% tsp = plot(torsoShp,'FaceAlpha',0.05);
% hold on


% apply rotation and translation to the stomach points
midPoint = mean(centrePoints,2);

plot_sims = 1:size(SID_params,1); %plot all sims)

for n = plot_sims
    
    [points_rotated, vectors_rotated] = transform_dipoles(centrePoints, tgt_vectors, ...
        SID_params(n,:), midPoint);
    
    %plot
    alph = 0.2;
    scatter3(points_rotated(1,:),points_rotated(2,:),points_rotated(3,:),'filled','MarkerEdgeAlpha',alph,'MarkerFaceAlpha',alph,'SizeData',5,'MarkerFaceColor','#0072BD')
    hold on
end