function [smallTorsoShp] = make_small_torso(torsoShp,margin)


%get a grid of points
mn = min(torsoShp.Points,[],1);
mx = max(torsoShp.Points,[],1);
nn = 30;
[X,Y,Z] = meshgrid(linspace(mn(1),mx(1),nn),linspace(mn(2),mx(2),nn),linspace(mn(3),mx(3),nn*2));

%remove points outside torso
gridPoints = [X(:),Y(:),Z(:)];
gridPoints = gridPoints(inShape(torsoShp,gridPoints),:);

%%
FV.vertices = torsoShp.Points;

FV.faces = boundaryFacets(torsoShp);
missingNodes = setdiff(1:size(FV.vertices,1),unique(FV.faces));

missingNodes = sort(missingNodes,'descend');
for i = 1:length(missingNodes)
    nd = missingNodes(i);

    FV.vertices(nd,:) = [];
    FV.faces(FV.faces > nd) = FV.faces(FV.faces > nd) - 1;
    
end

distances = point2trimesh(FV,'QueryPoints',gridPoints,'UseSubSurface',false);
%%

gridPoints = gridPoints(distances < - margin,:);
smallTorsoShp = alphaShape(gridPoints,100);


end