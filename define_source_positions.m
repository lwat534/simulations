%% Written by L. Watson
% Adapted from example_write_source.m written by R. Avco
% March 2021
% Source generation for electrophys simulations 
%%
clear;clc;close all;
addpath("functions")

make_plots=0;
%% Read stomach data (to use as a constraint in source definition)
fpath = 'stomach/stomach.exnode';
[FieldData] = extract_field_generic(fpath);
StomNodePos = FieldData.FieldValues;


fpath = 'stomach/stomach.exelem';
%elemNodeIds gives the nodes (with coords contained in field data)for the
%each element
[ElemIds, ElemNodeIds, ElemFaces] = extract_exelem(fpath);

%% read torso data
torsoPath = 'output/output1/torso.exnode';
[TorsoField] = extract_field_generic(torsoPath);
TorsoNodePos = TorsoField.FieldValues;


%make an 'alpha shape' defining all torso points
torsoShp = alphaShape(TorsoNodePos,150);

margin = 25;
smallTorsoShp = make_small_torso(torsoShp,margin);
%%
if make_plots
    figure()
    plot(smallTorsoShp,'FaceAlpha',0.3)
    hold on
    pause
    plot(torsoShp,'FaceAlpha',0.3)
end

%% find current max SID

SID_list = dir('input');
SIDs = zeros(length(SID_list),1);
for f = 1:length(SID_list)
    id = sscanf(SID_list(f).name,'dipole%d.ipsour');
    if ~isempty(id)
        SIDs(f) = id;
    end
end

SID_start = max(SIDs);
clear SIDs id SID_list


%% get centreline points and tangent vectors
nPoints = 29;

[centrePoints, tgt_vectors, centre_curve, deriv_spline] = ...
    get_centrepoints(ElemIds,ElemNodeIds,StomNodePos,nPoints);


%% apply rotation and translation to the stomach points
midPoint = mean(centrePoints,2);


%preallocate array for storing simulation params
nSims = 4125;
paramLog = zeros(nSims,8);

thp = 20;
dp = 50;

countcountcount = 0;

for n = 1:nSims
    SID = SID_start + n;
    
    while true
        %rotations from -20:20 degrees
        %translations from -50:50 mm
        theta_trans = rand(1,6) .* repelem(2 * [thp,dp],1,3) - repelem([thp,dp],1,3);

        %automatically have direction oriented in anterograde direction
        theta_trans = [theta_trans, 1];
    

        [points_rotated, vectors_rotated] = transform_dipoles(centrePoints, tgt_vectors, ...
            theta_trans, midPoint);
        %points_rotated = centrePoints;
        %vectors_rotated = tgt_vectors;
    
        %% check all points are within torso
        if all(inShape(smallTorsoShp,points_rotated'))
            

            %% Define a dipoles at each centre point on the stomach for each timestep
            % Locate a dipole on a selected stomach node
            nDipole = 1;
            nTime = nPoints;

            %preallocate
            DipPos = zeros(nDipole,3,nTime);
            DipVec = zeros(nDipole,3,nTime);

            for t = 1:nTime
                DipPos(nDipole,1:3,t) = points_rotated(:,t)';
                DipVec(nDipole,1:3,t) = vectors_rotated(:,t)'; 
            end

            %% Write the .ipsour file
            datain = struct();
            datain.fname = sprintf('input/dipole%d.ipsour',SID);
            datain.dipole_pos = DipPos;
            datain.dipole_vec = DipVec;
            write_ipsour_file(datain)
           
            %save param details
             paramLog(n,:) = [SID, theta_trans];
        break
        else
            countcountcount = countcountcount+1;
        end
    end
end
disp(countcountcount)% 

writematrix(paramLog,'SID_log.csv','WriteMode','append')