%% Written by L. Watson
% Adapted from example_write_source.m written by R. Avco
% March 2021
% Source generation for electrophys simulations
%%
clear;clc;close all;
addpath("functions")

%% Read stomach data
fpath = 'stomach/stomach.exnode';
[FieldData] = extract_field_generic(fpath);
StomNodePos = FieldData.FieldValues;


fpath = 'stomach/stomach.exelem';
%elemNodeIds gives the nodes (with coords contained in field data)for the
%each element
[ElemIds, ElemNodeIds, ElemFaces] = extract_exelem(fpath);

%% read torso data
torsoPath = 'output/output1/torso.exnode';
[TorsoField] = extract_field_generic(torsoPath);
TorsoNodePos = TorsoField.FieldValues;

%make an 'alpha shape' defining all torso points
torsoShp = alphaShape(TorsoNodePos,200);

%% make small torso

margin= 25;
smallTorsoShp = make_small_torso(torsoShp,margin);

%% read SID params

SID_params = readmatrix('SID_log.csv','NumHeaderLines',1);
SID_params = SID_params(:,2:end);

%% get first line of SID_params
fid = fopen('SID_log.csv');
headerLine = split(fgetl(fid),',');
headerLine = reshape(headerLine,[1,8]);
fclose(fid);

%% get centreline points and tangent vectors
nPoints = 29;

[centrePoints, tgt_vectors, centre_curve, deriv_spline] = ...
    get_centrepoints(ElemIds,ElemNodeIds,StomNodePos,nPoints);


%% iterate through sims and regenerate bad options


thp = 20;
dp = 50;

midPoint = mean(centrePoints,2);

check_idxs = 1:size(SID_params,1); %check all sims)

redo_list = false(size(check_idxs));

rng('shuffle')

for n = check_idxs
    SID = n;
    
    while true
    
        [points_rotated, vectors_rotated] = transform_dipoles(centrePoints, tgt_vectors, ...
            SID_params(n,:), midPoint);
        
        if all(inShape(smallTorsoShp,points_rotated'))
            
            %if they've been regenerated, rewrite input files
            if redo_list(n)
                %% Define a dipoles at each centre point on the stomach for each timestep
                % Locate a dipole on a selected stomach node
                nDipole = 1;
                nTime = nPoints;

                %preallocate
                DipPos = zeros(nDipole,3,nTime);
                DipVec = zeros(nDipole,3,nTime);

                for t = 1:nTime
                    DipPos(nDipole,1:3,t) = points_rotated(:,t)';
                    DipVec(nDipole,1:3,t) = vectors_rotated(:,t)'; 
                end

                %% Write the .ipsour file
                datain = struct();
                datain.fname = sprintf('input/dipole%d.ipsour',SID);
                datain.dipole_pos = DipPos;
                datain.dipole_vec = DipVec;
                write_ipsour_file(datain)                
                
            end            
            
            %exit loop
            break
        else
            redo_list(n) = 1;
            
            %regenerate params
            %rotations from -20:20 degrees
            %translations from -50:50 mm
            SID_params(n,1:(end-1)) = rand(1,6) .* repelem(2 * [thp,dp],1,3) - repelem([thp,dp],1,3);
        end
        
    end
end

disp(sum(redo_list))
%% rewrite SID log
clear SID
T = array2table([check_idxs', SID_params]);
T.Properties.VariableNames(1:8) = headerLine;
writetable(T,'SID_log.csv')

writematrix(find(redo_list)','rerun_params.csv')

           
 